FROM alpine:latest
ADD ./runme.sh /root/runme.sh
RUN chmod +x /root/runme.sh
CMD ["/root/runme.sh"]

